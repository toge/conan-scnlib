from conans import ConanFile, CMake, tools

import shutil

class ScnlibConan(ConanFile):
    name           = "scnlib"
    version        = "0.3"
    license        = "Apache-2.0"
    author         = "toge.mail@gmail.com"
    url            = "https://bitbucket.org/toge/conan-scnlib"
    description    = "scanf for modern C++ https://scnlib.dev"
    topics         = ("scanf")
    generators     = "cmake"

    def source(self):
        tools.get('https://github.com/eliaskosunen/scnlib/archive/v{}.zip'.format(self.version))
        shutil.move("scnlib-{}".format(self.version), "scnlib")

    def build(self):
        cmake = CMake(self)
        cmake.definitions["SCN_TESTS"]      = False
        # cmake.definitions["SCN_EXAMPLES"]   = False
        cmake.definitions["SCN_BENCHMARKS"] = False
        # cmake.definitions["SCN_DOCS"]       = False
        # cmake.definitions["SCN_INSTALL"]    = False
        cmake.configure(source_folder="scnlib")
        cmake.build()

    def package(self):
        self.copy("*.h"  , src="scnlib/include", dst="include", keep_path=True)
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.a"  , dst="lib", keep_path=False)    

    def package_info(self):
        self.cpp_info.libs = ["scn"]
