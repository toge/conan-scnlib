#include <iostream>
#include <string_view>

#include <scn/scn.h>

using namespace std::literals; // For sv

int main() {
    std::string word;
    scn::scan("Hello world!", "{}", word);

    std::cout << word << '\n'; // Will output "Hello"

    return 0;
}